import PIL.Image
import PIL.ExifTags

from datetime import datetime
from os import listdir
from os.path import isfile, join, splitext
from shutil import copyfile

from tkinter import *
from tkinter.filedialog import *

import argparse



def is_valid_picture_file(path):
    is_file = isfile(path)
    ext = splitext(path)[1].lower()
    # image_ext = ['.jpg', '.png', '.bmp', '.tiff', '.gif', '.cr2', '.dng']
    image_ext = ['.jpg']
    return is_file and (ext in image_ext)

def rename_copy(src, dest):
    if args.cpy:
        copyfile(src, dest)
    else:
        os.rename(src, dest)

def start_batch_rename():
    # files = [f for f in listdir(args.idir) if is_valid_picture_file(join(args.idir, f))]
    files = [f for f in listdir(args.idir) if isfile(join(args.idir, f))]

    for file in files:
        fullpath = join(args.idir, file)

        # check if the file is valid picture
        if not is_valid_picture_file(fullpath):
            output_filename = join(args.odir, file)
            print(output_filename)
            rename_copy(fullpath, output_filename)
            continue


        with PIL.Image.open(fullpath) as img:
            ex = img._getexif()
            img_filename = img.filename


        # No EXIF !
        if not ex:
            output_filename = join(args.odir, os.path.basename(img_filename))
            rename_copy(fullpath, output_filename)
            continue

        exif = {
            PIL.ExifTags.TAGS[k]: v
            for k, v in ex.items()
            if k in PIL.ExifTags.TAGS
        }

        str_datetime = exif.get('DateTime', '')
        camera_model = exif.get('Model', '').replace('\0', '')

        #fix_date = datetime.strptime(args.fix_date, "%Y%m%d-%H%M%S") if args.fix_date else None

        if str_datetime:
            date = datetime.strptime(str_datetime, "%Y:%m:%d %H:%M:%S")
            str_datetime_new_format = date.strftime("%Y%m%d-%H%M%S")
        else:
            str_datetime_new_format = ''

        name = '-' + args.name if args.name else ''

        i = 1

        output_filename = join(args.odir,
                               str_datetime_new_format + '-' + camera_model.strip() + name + '.jpg')

        while isfile(output_filename):
            i += 1
            output_filename = join(args.odir, str_datetime_new_format + '-' + camera_model.strip() + name + ' ' + str(i) + '.jpg')

        print(fullpath)
        print(' > ')
        print(output_filename)

        rename_copy(fullpath, output_filename)



def open_directory_dialog_idir():
    args.idir = askdirectory(title="Select input directory")
    input_idir.delete(0, END)
    input_idir.insert(0, args.idir)

def open_directory_dialog_odir():
    args.odir = askdirectory(title="Select output directory")
    input_odir.delete(0, END)
    input_odir.insert(0, args.odir)

def bind_entry_name(e):
    args.name = e.widget.get()

def bind_entry_idir(e):
    args.idir = e.widget.get()

def bind_entry_odir(e):
    args.odir = e.widget.get()

def bind_checkbox_copy():
    args.cpy = checkbox_copy_var.get()




parser = argparse.ArgumentParser("batch rename")
parser.add_argument("gui", help="Display GUI", type=bool, nargs='?')
parser.add_argument("-idir", help="Input image directory", type=str)
parser.add_argument("-odir", help="Output image directory", type=str)
parser.add_argument("-name", help="Suffix name for images", type=str)
parser.add_argument("-fix_date", help="Date of image capture fix", type=str)
parser.add_argument("-cpy", help="Copy file", type=bool)

args = parser.parse_args()


# gui configuration
if args.gui or (not args.idir):
    # window
    window = Tk()
    window.geometry("400x350")
    window.rowconfigure(0, weight=1)
    window.columnconfigure(0, weight=1)
    window.title("PyPic batch rename")

    # Input directory selection
    input_idir = Entry(window)
    input_idir.insert(0, args.idir if args.idir else "")
    input_idir.bind("<KeyRelease>", bind_entry_idir)
    input_idir.grid(row=0, column=0, sticky="new")
    btn_idir = Button(window, text="Choose...", command=open_directory_dialog_idir)
    btn_idir.grid(row=0, column=1, sticky="nw")



    # Output directory selection
    input_odir = Entry(window)
    input_odir.insert(0, args.odir if args.odir else "")
    input_odir.bind("<KeyRelease>", bind_entry_odir)
    input_odir.grid(row=1, column=0, sticky="ew")
    btn_odir = Button(window, text="Choose...", command=open_directory_dialog_odir)
    btn_odir.grid(row=1, column=1, sticky="w")

    # Copy checkbox
    checkbox_copy_var = BooleanVar()
    checkbox_copy_var.set(bool(args.cpy))

    checkbox_copy = Checkbutton(window, text="Copy ?", variable=checkbox_copy_var, onvalue=True, offvalue=False, command=bind_checkbox_copy)
    checkbox_copy.grid(row=2, columnspan=2, sticky="ne")

    # Name selection
    input_name = Entry(window)
    input_name.grid(row=3, sticky="ew", columnspan=2)
    input_name.bind("<KeyRelease>", bind_entry_name)

    # Run button
    btn_rename = Button(window, text="Start batch rename", command=start_batch_rename)
    btn_rename.grid(row=3, sticky="nsew", columnspan=2)

    window.mainloop()
    exit(0)
else:
    start_batch_rename()
